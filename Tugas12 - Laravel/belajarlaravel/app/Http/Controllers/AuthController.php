<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view ('register');
    }

    public function welcome(Request $request)
    {
        // dd($request->all());
        $firstN = $request['fname'];
        $lastN = $request['lname'];
        
        return view('welcome', ['firstN' => $firstN, 'lastN' => $lastN]);
    }
}
