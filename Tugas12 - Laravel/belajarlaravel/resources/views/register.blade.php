@extends('layouts.master')

@section('title')
    Register
@endsection

@section('sub-title')
    
@endsection

@section('content')
<h1>Buat Akun Baru!</h1>
<h2>Sign Up Form</h2>

<div>
<form action="welcome" method="POST">
    @csrf
    <label for="fname">First name:</label><br>
    <input type="text" id="fname" name="fname" value=""><br>
    <label for="lname">Last name:</label><br>
    <input type="text" id="lname" name="lname" value=""><br><br>
    Gender:<br>
    <input type="radio" id="html" name="fav_language" value="HTML">
    <label for="male">Male</label><br>
    <input type="radio" id="female" name="fav_language" value="CSS">
    <label for="Female">Female</label><br>
    <input type="radio" id="other" name="fav_language" value="JavaScript">
    <label for="other">Other</label><br><br>
    Nationality:<br>
    <select name="nation" id="nation">
        <option value="indonesia">Indonesia</option>
        <option value="singapore">Singapore</option>
        <option value="malaysia">Malaysia</option>
        <option value="australia">Australia</option>
    </select><br><br>
    Language Spoken:<br>
    <input type="checkbox" id="bahasa" name="bahasa" value="Bike">
    <label for="bahasa">Bahasa Indonesia</label><br>
    <input type="checkbox" id="english" name="english" value="Car">
    <label for="english">English</label><br>
    <input type="checkbox" id="Other" name="Other" value="Boat">
    <label for="Other">Other</label>
    <br><br>
    Bio:<br>
    <textarea id="bio" name="bio" rows="4" cols="50"></textarea>
    <br>
    <input type="submit" value="Submit">
    <input type="reset">
</form> 
</div>
@endsection


@section('footer')
    Form Register
@endsection