@extends('layouts.master')

@section('title')
    Welcome
@endsection

@section('sub-title')
    
@endsection

@section('content')
<h1>Selamat Datang! {{ $firstN }} {{ $lastN }}.</h1>
<br><br>
<h3>Terima kasih telah bergabung di SanberBook. Social Media kita bersama!</h3>
@endsection

@section('footer')
    Selamat Datang
@endsection