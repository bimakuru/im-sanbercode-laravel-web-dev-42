@extends('layouts.master')

@section('title')
    Halaman Detail Data Cast
@endsection

@section('sub-title')
    View Detail Data Cast
@endsection


@section('content')

<h1>{{ $cast->name }}</h1>
<p>{{ $cast->umur }} tahun</p>
<p>{{ $cast->bio }}</p>

<a href="/cast" class="btn btn-secondary btn-sm">Kembali</a>

@endsection