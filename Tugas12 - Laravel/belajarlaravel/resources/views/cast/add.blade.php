@extends('layouts.master')

@section('title')
    Cast
@endsection

@section('sub-title')
    Create New Cast
@endsection

{{-- @push('scripts')
<script src="{{asset('template/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{asset('template/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush

@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.13.2/datatables.min.css"/>
@endpush --}}

@section('content')


<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <label>Nama Cast</label>
      <input type="text" name="name" class="form-control">
    </div>
        @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
      <label>Umur Cast</label>
      <input type="text" name="umur" class="form-control">
    </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
        <label>Bio Cast</label>
        <textarea type="text" name="bio" class="form-control" cols="5" rows="5"></textarea>
    </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>


@endsection