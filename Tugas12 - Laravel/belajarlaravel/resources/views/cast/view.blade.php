@extends('layouts.master')

@section('title')
    Halaman Tampil Cast
@endsection

@section('sub-title')
    View Data Cast
@endsection


@section('content')

<a href="/cast/create" class="btn btn-primary btn-sm my-2">Add New Cast</a>

<table class="table">
    <thead class="thead-dark">
        <th>#</th>
        <th>Name</th>
        <th>Action</th>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
            <tr>
                <td>{{ $key +1 }}</td>
                <td>{{ $item -> name }}</td>
                <td>
                    <form action="/cast/{{ $item->id }}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="/cast/{{ $item->id }}" class="btn btn-primary btn-sm">Detail</a>
                        <a href="/cast/{{ $item->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                        <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td>Data Cast Kosong</td>
            </tr>
        @endforelse
    </tbody>
</table>

@endsection