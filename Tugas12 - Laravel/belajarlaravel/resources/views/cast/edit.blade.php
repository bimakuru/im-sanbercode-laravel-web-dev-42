@extends('layouts.master')

@section('title')
    Edit data Cast
@endsection

@section('sub-title')
    Edit data Cast
@endsection



@section('content')


<form action="/cast/{{ $cast->id }}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama Cast</label>
      <input type="text" name="name" value="{{ $cast->name }}" class="form-control">
    </div>
        @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
      <label>Umur Cast</label>
      <input type="text" name="umur" value="{{ $cast->umur }}" class="form-control">
    </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
        <label>Bio Cast</label>
        <textarea type="text" name="bio" class="form-control" cols="5" rows="5">{{ $cast->bio }}</textarea>
    </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        
        <a href="/cast" class="btn btn-secondary btn-sm">Kembali</a>
        <button type="submit" class="btn btn-primary btn-sm">Submit</button>
    
  </form>


@endsection