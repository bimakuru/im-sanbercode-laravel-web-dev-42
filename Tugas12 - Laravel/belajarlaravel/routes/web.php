<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class,'dashboard'] );

Route::get('/register', [AuthController::class,'register'] );

Route::post('/welcome', [AuthController::class,'welcome'] );



Route::get('/master', function(){
    return view('layouts.master');
}  );


Route::get('/data-tables', function(){
    return view('data-tables');
}  );

Route::get('/table', function(){
    return view('table');
}  );

// CRUD Cast

//Create data Cast
Route::get('/cast/create', [CastController::class, 'create'] );

//Save Store data Cast
Route::post('/cast', [CastController::class, 'store'] );


//Read all data Cast
Route::get('/cast', [CastController::class, 'index']);


//Detail data Cast by id
Route::get('/cast/{id}', [CastController::class, 'show']);


//Form Edit data Cast by id
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);

//Edit data Cast by id
Route::put('cast/{id}', [CastController::class, 'update']);

//Delete data Cast
Route::delete('cast/{id}', [CastController::class, 'destroy']);