<?php 

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("shaun");

echo "Name : <b>" . $sheep->name . "</b><br>"; // "shaun"
echo "Legs : " . $sheep->legs . "<br>"; // 4
echo "Cold Blooded : " . $sheep->cold_blooded . "<br>"; // "no"
echo "<br>";

$kodok = new Frog("buduk");

echo "Name : <b>" . $kodok->name . "</b><br>"; 
echo "Legs : " . $kodok->legs . "<br>"; 
echo "Cold Blooded : " . $kodok->cold_blooded . "<br>";
echo "Yell : " . $kodok->jump . "<br>";
echo "<br>";

$sungokong = new Ape("kera sakti");

echo "Name : <b>" . $sungokong->name . "</b><br>"; 
echo "Legs : " . $sungokong->legs . "<br>"; 
echo "Cold Blooded : " . $sungokong->cold_blooded . "<br>";
echo "Yell : " . $sungokong->yell . "<br>";
echo "<br>";

?>